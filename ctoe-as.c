#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "ctoe-as.h"

static void parse_instruction(char *instruction, char **opp_code, char **reg,
			      char **value)
{
	int i = 0;
	/* INSTEAD OF A HUGE PARSE FUNCTION, I'M USING STRTOK */
	char *str[3] = { NULL };
	str[i] = strtok(instruction, " ");
	i++;
	while (i < 3) {
		str[i] = strtok(NULL, " ");
		i++;
	}
	/* SET UP THE ARUGMENTS */
	*opp_code = str[0];
	*reg = str[1];
	*value = str[2];

	if (*opp_code == NULL) {
		fprintf(stderr, "failed to set opp_code\n");
		exit(EXIT_FAILURE);
	}

	if (*reg == NULL) {
		fprintf(stderr, "failed to set reg\n");
		exit(EXIT_FAILURE);
	}

	if (*value == NULL) {
		fprintf(stderr, "failed to set vlaue\n");
		exit(EXIT_FAILURE);
	}
	/* GET THE BINARY FOR THE OP CODE AND THE INSTRUCTION */
}

#define IO_ADDRESS_SHIFT 3
static unsigned char *direct_I_O_addressing(char *instruction)
{
	char *opp_code = NULL; /* THE INSTRUCTIONS ITSEF */
	char *reg = NULL; /* THE REGISTER */
	char *value = NULL; /* THE VALUE */
	unsigned char *bin;
	parse_instruction(instruction, &opp_code, &reg, &value);

	bin = malloc(sizeof(*bin) * 2);
	if (bin == NULL) {
		printf("Failed to allocated bin : %s\n", __func__);
		exit(EXIT_FAILURE);
	}

	unsigned int bit_position;
	long io_address;

	if (strcmp(opp_code, "cbi") == 0)
		bin[0] = 0b10011000;
	else if (strcmp(opp_code, "sbic") == 0)
		bin[0] = 0b10011001;
	else if (strcmp(opp_code, "sbi") == 0)
		bin[0] = 0b10011010;
	else if (strcmp(opp_code, "sbis") == 0)
		bin[0] = 0b10011011;
	else {
		perror("Instruction doesn't exists");
		printf("This is what failed: %s\n", opp_code);
		exit(1);
	}

	/*  POPULATE BIN WITH THE SECOND 8 BITS */
	io_address = strtol(reg, NULL, 16);
	bit_position = (value[0] - '0');
	bin[1] = (io_address << IO_ADDRESS_SHIFT) | bit_position;

	return bin;
}

static unsigned char *constant_code_instruction(char *instruction)
{
	unsigned char *bin;
	char *token = strtok(instruction, " ");
	bin = malloc(sizeof(*bin) * 2);
	if (bin == NULL) {
		printf("Failed to allocated bin: %s\n", __func__);
		exit(EXIT_FAILURE);
	}
	if (strcmp(token, "clc") == 0) {
		bin[0] = 0b10010100;
		bin[1] = 0b10001000;
	} else if (strcmp(token, "clh") == 0) {
		bin[0] = 0b10010100;
		bin[1] = 0b11011000;
	} else if (strcmp(token, "cli") == 0) {
		bin[0] = 0b10010100;
		bin[1] = 0b11111000;
	} else if (strcmp(token, "cln") == 0) {
		bin[0] = 0b10010100;
		bin[1] = 0b10101000;
	} else if (strcmp(token, "cls") == 0) {
		bin[0] = 0b10010100;
		bin[1] = 0b11001000;
	} else if (strcmp(token, "clt") == 0) {
		bin[0] = 0b10010100;
		bin[1] = 0b11101000;
	} else if (strcmp(token, "clv") == 0) {
		bin[0] = 0b10010100;
		bin[1] = 0b10111000;
	} else if (strcmp(token, "clz") == 0) {
		bin[0] = 0b10010100;
		bin[1] = 0b10011000;
	} else if (strcmp(token, "eicall") == 0) {
		bin[0] = 0b10010101;
		bin[1] = 0b00011001;
	} else if (strcmp(token, "eijmp") == 0) {
		bin[0] = 0b10010100;
		bin[1] = 0b00011001;
	} else if (strcmp(token, "ijmp") == 0) {
		bin[0] = 0b10010100;
		bin[1] = 0b00001001;
	} else if (strcmp(token, "nop") == 0) {
		bin[0] = 0b00000000;
		bin[1] = 0b00000000;
	} else if (strcmp(token, "ret") == 0) {
		bin[0] = 0b10010101;
		bin[1] = 0b00001000;
	} else if (strcmp(token, "reti") == 0) {
		bin[0] = 0b10010101;
		bin[1] = 0b00011000;
	} else if (strcmp(token, "sec") == 0) {
		bin[0] = 0b10010100;
		bin[1] = 0b00001000;
	} else if (strcmp(token, "seh") == 0) {
		bin[0] = 0b10010100;
		bin[1] = 0b01011000;
	} else if (strcmp(token, "sei") == 0) {
		bin[0] = 0b10010100;
		bin[1] = 0b01111000;
	} else if (strcmp(token, "sen") == 0) {
		bin[0] = 0b10010100;
		bin[1] = 0b00101000;
	} else if (strcmp(token, "ses") == 0) {
		bin[0] = 0b10010100;
		bin[1] = 0b01001000;
	} else if (strcmp(token, "set") == 0) {
		bin[0] = 0b10010100;
		bin[1] = 0b01101000;
	} else if (strcmp(token, "sev") == 0) {
		bin[0] = 0b10010100;
		bin[1] = 0b00111000;
	} else if (strcmp(token, "sez") == 0) {
		bin[0] = 0b10010100;
		bin[1] = 0b00011000;
	} else if (strcmp(token, "sleep") == 0) {
		bin[0] = 0b10010101;
		bin[1] = 0b10001000;
	} else if (strcmp(token, "wdr") == 0) {
		bin[0] = 0b10010101;
		bin[1] = 0b10101000;
	} else {
		printf("Instruction doesn't exists: %d\n", __LINE__);
		exit(EXIT_FAILURE);
	}
	return bin;
}

unsigned char *instruction_table(char *instruction)
{
	char *instr = NULL, *temp = NULL;
	unsigned char *encoded_instruction = NULL;
	temp = strdup(instruction);
	instr = strtok(temp, " ");
	if (instr == NULL) {
		printf("Note a valid instruction, please double check your code\n");
		exit(EXIT_FAILURE);
	}
	if (strcmp(instr, "cbi") == 0)
		encoded_instruction = direct_I_O_addressing(instruction);
	else if (strcmp(instr, "sbic") == 0)
		encoded_instruction = direct_I_O_addressing(instruction);
	else if (strcmp(instr, "sbi") == 0)
		encoded_instruction = direct_I_O_addressing(instruction);
	else if (strcmp(instr, "sbis") == 0)
		encoded_instruction = direct_I_O_addressing(instruction);
	else if (strcmp(instr, "clc") == 0)
		encoded_instruction = constant_code_instruction(instruction);
	else if (strcmp(instr, "clh") == 0)
		encoded_instruction = constant_code_instruction(instruction);
	else if (strcmp(instr, "cli") == 0)
		encoded_instruction = constant_code_instruction(instruction);
	else if (strcmp(instr, "cln") == 0)
		encoded_instruction = constant_code_instruction(instruction);
	else if (strcmp(instr, "cls") == 0)
		encoded_instruction = constant_code_instruction(instruction);
	else if (strcmp(instr, "clt") == 0)
		encoded_instruction = constant_code_instruction(instruction);
	else if (strcmp(instr, "clv") == 0)
		encoded_instruction = constant_code_instruction(instruction);
	else if (strcmp(instr, "clz") == 0)
		encoded_instruction = constant_code_instruction(instruction);
	else if (strcmp(instr, "ijmp") == 0)
		encoded_instruction = constant_code_instruction(instruction);
	else if (strcmp(instr, "nop") == 0)
		encoded_instruction = constant_code_instruction(instruction);
	else if (strcmp(instr, "ret") == 0)
		encoded_instruction = constant_code_instruction(instruction);
	else if (strcmp(instr, "reti") == 0)
		encoded_instruction = constant_code_instruction(instruction);
	else if (strcmp(instr, "sec") == 0)
		encoded_instruction = constant_code_instruction(instruction);
	else if (strcmp(instr, "seh") == 0)
		encoded_instruction = constant_code_instruction(instruction);
	else if (strcmp(instr, "sei") == 0)
		encoded_instruction = constant_code_instruction(instruction);
	else if (strcmp(instr, "sen") == 0)
		encoded_instruction = constant_code_instruction(instruction);
	else if (strcmp(instr, "ses") == 0)
		encoded_instruction = constant_code_instruction(instruction);
	else if (strcmp(instr, "set") == 0)
		encoded_instruction = constant_code_instruction(instruction);
	else if (strcmp(instr, "sev") == 0)
		encoded_instruction = constant_code_instruction(instruction);
	else if (strcmp(instr, "sez") == 0)
		encoded_instruction = constant_code_instruction(instruction);
	else if (strcmp(instr, "sleep") == 0)
		encoded_instruction = constant_code_instruction(instruction);
	else if (strcmp(instr, "wdr") == 0)
		encoded_instruction = constant_code_instruction(instruction);
	free(temp);
	return encoded_instruction;
}
