# ctoe assembler

## Usage

```
Usage: ctoe-as [options] asm-file

Options:
-d, --debug              produce assembler debugging messages
-o, --output <file>      place the output into <file>
```

## Development
### Requirements

```bash
# BuildRequires
$ yum install gcc make

# Developer helpers
$ yum install \
    avr-binutils \
    avr-gcc \
    avr-gcc-c++ \
    avr-libc \
    avr-libc-doc \
    clang-tools-extra \
    diffutils \
    git \
    valgrind
```

### Coding Conventions

The `ctoe-as` project follows [Linux kernel coding
style](https://www.kernel.org/doc/html/v4.10/process/coding-style.html).
Coding convention is not enforced by any tooling, however the project
does leverage the kernel's
[clang-format](https://www.kernel.org/doc/html/latest/process/clang-format.html)
file to help automate formatting.  Reports from *clang-format* are
considered suggestions and not to be blindly applied.  The more the code
follows its formatting, though, the easier it will be read.
