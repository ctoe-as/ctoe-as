CC=gcc
CFLAGS=-g -Wall
TARGET=ctoe-as
DEPS=ctoe-as.h
OBJECTS = main.o ctoe-as.o
CLANG_FORMAT_DIFF=/usr/share/clang/clang-format-diff.py

$(TARGET): $(OBJECTS)
	$(CC) $(CFLAGS) -o $(TARGET) $(OBJECTS)
main.o: main.c $(DEPS) 
	$(CC) $(CFLAGS) -c main.c -o main.o
ctoe-as.o: ctoe-as.c $(DEPS)
	$(CC) $(CFLAGS) -c ctoe-as.c -o ctoe-as.o
clang-format-branch:
	@git diff remotes/origin/main.. -U0 --no-color | \
	  $(CLANG_FORMAT_DIFF) -p1 | \
	  awk 'NR==1 { rc=1 } { print $0 } END { exit(rc) }'
clang-tidy: *.c
	clang-tidy --quiet -header-filter=.* -checks=linuxkernel*,-clang-analyzer-security.insecureAPI.DeprecatedOrUnsafeBufferHandling -warnings-as-errors=* *.c --
help: 
	@echo "Targets: "
	@echo -e "\tmake clean\t\t\tremoves all executables and object files"
	@echo -e "\tmake ctoe-as\t\t\tbuilds the program"
	@echo -e ""
	@echo "Development Targets: "
	@echo -e "\tmake clang-format-branch\trun source code formatting check"
	@echo -e "\tmake clang-tidy\t\t\tcall clang-tidy linter tool"
	@echo -e "\tmake tests\t\t\trun unit tests"
	@echo -e "\tmake valgrind-tests\t\trun unit tests thru valgrind"

tests: $(TARGET)
	make -s -C tests/

valgrind-tests: $(TARGET)
	make tests AS="valgrind --quiet --show-error-list=yes --error-exitcode=1 --leak-check=full ../ctoe-as"

.PHONY: clean clang-format-branch clang-tidy help tests valgrind-tests
	
clean:
	make -C tests/ clean
	$(RM) $(TARGET) *.o

