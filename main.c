#include <stdio.h>
#include <getopt.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include "ctoe-as.h"

bool debug;

void usage()
{
	printf("\nUsage: ctoe-as [options] asm-file\n\n Option:\n");
	printf("-d, --debug\t\tproduce assembler debugging message\n");
	printf("-o, --output\tplace the output into <file>\n");

	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	struct option longopts[] = { { "help", no_argument, NULL, 'h' },
				     { "debug", no_argument, NULL, 'd' },
				     { "output", required_argument, NULL, 'o' },
				     { 0, 0, 0, 0 } };

	char *output_file = NULL;
	char *input_file = NULL;
	unsigned char *encoded_instruction;
	int opt;
	FILE *input_fp, *output_fp;
	size_t len = 100;
	char buf[len];

	if (argc < 2) {
		usage();
	}

	while ((opt = getopt_long(argc, argv, "-:hdo:", longopts, NULL)) !=
	       -1) {
		switch (opt) {
		case 'h':
			usage();
			break;
		case 'd':
			debug = true;
			break;
		case 'o':
			output_file = optarg;
			break;
		case 1:
			input_file = optarg;
			break;
		default:
			usage();
			break;
		}
	}

	if (input_file == NULL) {
		fprintf(stderr, "please specify an input file\n");
		exit(EXIT_FAILURE);
	}

	if (output_file == NULL) {
		fprintf(stderr, "please specify an input file\n");
		exit(EXIT_FAILURE);
	}

	/* VALIDATE I/0 FILES */
	if ((input_fp = fopen(input_file, "r")) == NULL) {
		perror("Can't open input file");
		exit(1);
	}

	if ((output_fp = fopen(output_file, "w")) == NULL) {
		perror("Can't open output file");
		exit(2);
	}

	/* READ FROM THE INPUT FILE */
	while (fgets(buf, len, input_fp)) {
		if (buf[0] == ';') {
			continue;
		} else if (buf[0] == '\n') {
			continue;
		}

		else {
			/* CALL THE ENCODER FUNCTION */
			encoded_instruction = instruction_table(buf);
			/* WRITE ENCODED INSTRUCTIONS TO THE OUTPUT FILE */
			fprintf(output_fp, "%c%c", encoded_instruction[1],
				encoded_instruction[0]);
			/* UNSET ALL THE BITS */
			encoded_instruction[0] = encoded_instruction[1] = 0;
		}
	}

	/* CLOSE THE INPUT AND OUPUT FILES */
	fclose(input_fp);
	fclose(output_fp);

	return EXIT_SUCCESS;
}
